const express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    mongoose = require('mongoose'),
    cors = require('cors')



app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())


const Object3D = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    coors: {
        type: Object,
        required: true
    }
})

const ObjectsModel = mongoose.model('Object', Object3D)

app.get('/object', (req, res) => {
    ObjectsModel.find()
        .exec((err, results) => {
            if (err)
                return res.status(500).send({ message: 'Error', err })
            return res.send(results)
        })
})

app.get('/object/:filter', (req, res) => {
    ObjectsModel.find({ name: { $regex: '.*' + req.params.filter + '.*' } })
        .exec((err, objects) => {
            if (err)
                return res.status(500).send({ err })
            return res.send(objects)
        })
})

app.post('/object', (req, res) => {
    const newObject3D = new ObjectsModel({
        name: req.body.name,
        path: req.body.path,
        coors: req.body.coors
    })
    newObject3D.save((err, object3D) => {
        if (err)
            return res.status(500).send({ err })
        return res.send(object3D)
    })
})


mongoose.connect('mongodb+srv://mesterlum:palafox88@fco-kpxrm.mongodb.net/herman-follow?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useCreateIndex: true
    },
    err => {
        if (err)
            return Throw(err)
        app.listen(3000, () => {
            console.log('server running')
        })

    }
)